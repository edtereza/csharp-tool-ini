﻿using System;
using System.Reflection;
using System.Text;
using System.Runtime.InteropServices;

namespace Tools
{
    public static class INI
    {
        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        static extern long WritePrivateProfileString(String Section, String Key, String Value, String FilePath);

        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        static extern int GetPrivateProfileString(String Section, String Key, String Default, StringBuilder RetVal, int Size, String FilePath);

        public static String File = Assembly.GetEntryAssembly().GetName().Name;
        private static String _Path
        {
            get
            {
                String _Path = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                if (!_Path.EndsWith(@"\"))
                {
                    _Path = _Path + @"\";
                }
                if (!INI.File.EndsWith(@".ini"))
                {
                    INI.File = INI.File + @".ini";
                }
                return _Path + INI.File;
            }
        }

        public static String String(String Session, String Key, String Default = null)
        {
            StringBuilder _StringBuilder = new StringBuilder(255);
            try
            {
                GetPrivateProfileString(Session, Key, Default, _StringBuilder, 255, INI._Path);
            }
            catch { }
            return ((_StringBuilder.ToString() == "") ? Default : _StringBuilder.ToString());
        }
        public static Boolean Boolean(String Session, String Key, Boolean Default = false)
        {
            String _String = INI.String(Session, Key, null);
            if (_String == "True" || _String == "true" || _String == "T" || _String == "1")
                return true;
            else if (_String == "False" || _String == "false" || _String == "F" || _String == "0")
                return false;
            return Default;
        }
        public static Int32 Int32(String Session, String Key, Int32 Default = 0)
        {
            String _String = INI.String(Session, Key, null);
            Int32 _Int32 = Default;
            if (_String != null)
            {
                System.Int32.TryParse(_String, out _Int32);
            }
            return _Int32;
        }
        public static Int64 Int64(String Session, String Key, Int64 Default = 0)
        {
            String _String = INI.String(Session, Key, null);
            Int64 _Int64 = Default;
            if (_String != null)
            {
                System.Int64.TryParse(_String, out _Int64);
            }
            return _Int64;
        }
        public static void Write(String Session, String Key, String Value)
        {
            try
            {
                WritePrivateProfileString(Session, Key, Value, INI._Path);
            }
            catch { }
        }
        public static void Write(String Session, String Key, Int32 Value)
        {
            INI.Write(Session, Key, Value.ToString());
        }
        public static void Write(String Session, String Key, Int64 Value)
        {
            INI.Write(Session, Key, Value.ToString());
        }
        public static void Write(String Session, String Key, Decimal Value)
        {
            INI.Write(Session, Key, Value.ToString());
        }
        public static void Delete(String Session, String Key)
        {
            try
            {
                WritePrivateProfileString(Session, Key, null, INI._Path);
            }
            catch { }
        }
        public static Boolean Exists(String Session, String Key)
        {
            String _String = INI.String(Session, Key, null);
            return (_String == null ? false : true);
        }
    }
}
