﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// As informações gerais sobre um assembly são controladas por
// conjunto de atributos. Altere estes valores de atributo para modificar as informações
// associada a um assembly.
[assembly: AssemblyTitle("Tools / INI")]
[assembly: AssemblyDescription("Shared DLL for INI management.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Erik Douglas Tereza <erik.tereza@edtereza.com.br>")]
[assembly: AssemblyProduct("Tools / INI")]
[assembly: AssemblyCopyright("Erik Douglas Tereza <erik.tereza@edtereza.com.br> © 2018")]
[assembly: AssemblyTrademark("Erik Douglas Tereza <erik.tereza@edtereza.com.br>")]
[assembly: AssemblyCulture("")]

// Definir ComVisible como false torna os tipos neste assembly invisíveis
// para componentes COM. Caso precise acessar um tipo neste assembly de
// COM, defina o atributo ComVisible como true nesse tipo.
[assembly: ComVisible(false)]

// O GUID a seguir será destinado à ID de typelib se este projeto for exposto para COM
[assembly: Guid("d9783e92-3fcb-4c1f-8177-3f64a3a3bc59")]

// As informações da versão de um assembly consistem nos quatro valores a seguir:
//
//      Versão Principal
//      Versão Secundária 
//      Número da Versão
//      Revisão
//
// É possível especificar todos os valores ou usar como padrão os Números de Build e da Revisão
// usando o '*' como mostrado abaixo:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
